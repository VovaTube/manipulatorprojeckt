import { Component, OnInit } from '@angular/core';
import { Card } from '../Models/cartmodel';
import { Guid } from "guid-typescript";
import { BasketService } from '../servises/basket.service';

@Component({
  selector: 'app-out-products',
  templateUrl: './out-products.component.html',
  styleUrls: ['./out-products.component.css']
})
export class OutProductsComponent implements OnInit {
  public avalibleprodacts:Card [] = [];
  private wantToBuy:Card [] = []; 
  
  constructor(public userbasket: BasketService) {
    userbasket.countItemsInBasket.subscribe(value=>{
      console.log(value);
    })
   }

  ngOnInit(): void {
    this.avalibleprodacts=this.generateProducts();
  }
  generateProducts(){
    let cards =[];
   
    for (let index = 0; index < 2000; index++) {
        let card: Card = new Card();
        card.cardFooter = "5000";
        card.cardText = "Повний опис теестового продукту!!!"
        card.cardTitle = 'Тестовий продукт';
        card.price = 1450;
        card.cardId = Guid.create();
        card.srcImage= [];
        card.folderImageNane = 'circlebeton';
        card.srcImage.push( `..//..//assets/products/${card.folderImageNane}/${1}.jpg`);
        card.srcImage.push( '..//..//assets/products/circlebeton/2.jpg');
        card.srcImage.push( '..//..//assets/products/circlebeton/3.jpg');
        card.properties = [];
        card.properties.push({key:"Діаметр", value:"1,50 м"})
        card.properties.push({key:"Висота", value:"1,5 м"})
        card.properties.push({key:"Вага", value:"60 кг"})
        card.properties.push({key:"Матеріл", value:"бетон"})
        if (index%2==0) {
          card.discont=true
        }
        cards.push(card);
   }
   return cards;
  }

  addToUserBacket(items:Card){
    var inteminbasketflag;
    for (let index = 0; index < this.userbasket.orderItems.length; index++) {
      if(this.userbasket.orderItems[index].itemscard.cardId==items.cardId){
        inteminbasketflag = true;
      }
    }
    if (!inteminbasketflag) {
      this.userbasket.addToBasket(1,items);
    }
    else{
      this.userbasket.deleteFromBasket(items.cardId);
    }
  }

  itemInBasketFlag(id:any):boolean{
    for (let index = 0; index < this.userbasket.orderItems.length; index++) {
      if(this.userbasket.orderItems[index].itemscard.cardId==id){
       return true
      }
      }
    return false;
  }
}
