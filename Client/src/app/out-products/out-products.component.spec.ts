import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutProductsComponent } from './out-products.component';

describe('OutProductsComponent', () => {
  let component: OutProductsComponent;
  let fixture: ComponentFixture<OutProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
