import { BasketModel } from "./BasketModel";
import { Buyer } from "./BuyerModel";

export class Order {
    buyer:Buyer;
    buyerBasket:BasketModel;
    constructor(men: Buyer, menbasket:BasketModel){
        this.buyer=men;
    this.buyerBasket = menbasket;
    }
}