export  class   Card
{
  public cardId:any;
  public cardTitle:string;
  public cardText:string;
  public cardFooter: string; 
  public price:number;
  public properties: Array<{key:string, value:string}>
  public srcImage:string[];
  public folderImageNane:string;
  public discont:boolean;
}