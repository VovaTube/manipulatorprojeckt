import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasketBuyerComponent } from './basket-buyer/basket-buyer.component';
import { MainPageComponentComponent } from './main-page-component/main-page-component.component';
import { OurContactsComponent } from './our-contacts/our-contacts.component';
import { OurServicesComponent } from './our-services/our-services.component';
import { OutProductsComponent } from './out-products/out-products.component';

const routes: Routes = [
    { path: '', redirectTo: '/main', pathMatch: 'full' },
    { path: 'main', component: MainPageComponentComponent},
    { path: 'servises', component: OurServicesComponent},
    { path: 'pricelist', component: OurServicesComponent},
    { path: 'contacts', component: OurContactsComponent},
    { path: 'products', component: OutProductsComponent},
    { path: 'basket', component: BasketBuyerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
