import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiEndPointsService {
  private endpoints: Endpoints = null;
  private http: HttpClient;
  private assetsDirectory = 'assets';
  constructor(handler: HttpBackend) {
    this.http = new HttpClient(handler);
  }
  public get baseUrl(): string { return this.endpoints.baseUrl; }
    public get accountUrl(): string {return this.endpoints.clientUrl; }

    load(){
      return new Promise((resolve, rejects) =>{
        this.http.get(this.assetsDirectory + '/endpoints.json')
        .pipe(
         map((result: Endpoints) =>{
           this.endpoints = result;
           resolve(true);
         })).subscribe();
      })
    }

}
export class Endpoints {
  baseUrl: string;
  clientUrl: string;
}