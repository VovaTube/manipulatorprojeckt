import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BasketModel } from '../Models/BasketModel';
import { Card } from '../Models/cartmodel';

@Injectable({
  providedIn: 'root'
})
export class BasketService {
  orderItems:BasketModel[] = [];
  countItemsInBasket:BehaviorSubject<number> = new BehaviorSubject(0);
  constructor() {
   }

  addToBasket(count:number,orderitems:Card){
    var item =  new BasketModel(count, orderitems);
    this.orderItems.push(item);
    this.countItemsInBasket.next(this.countItemsInBasket.getValue()+1);
  }

  deleteFromBasket(id:number){
    for (let index = 0; index < this.orderItems.length; index++) {
      if(this.orderItems[index].itemscard.cardId==id){
        this.orderItems.splice(index,1);
        this.countItemsInBasket.next(this.countItemsInBasket.getValue()-1);
        break;
      }
    }
  }
  
}
