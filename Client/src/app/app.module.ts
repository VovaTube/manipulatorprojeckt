import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MainPageComponentComponent } from './main-page-component/main-page-component.component';
import { OurServicesComponent } from './our-services/our-services.component';
import { OurPricesComponent } from './our-prices/our-prices.component';
import { OurContactsComponent } from './our-contacts/our-contacts.component';
import { LazyLoadImageModule, LAZYLOAD_IMAGE_HOOKS, ScrollHooks } from 'ng-lazyload-image';
import { OutProductsComponent } from './out-products/out-products.component'; // <-- include ScrollHooks
import { ApiEndPointsService } from './servises/api-end-points.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BasketBuyerComponent } from './basket-buyer/basket-buyer.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainPageComponentComponent,
    OurServicesComponent,
    OurPricesComponent,
    OurContactsComponent,
    OutProductsComponent,
    BasketBuyerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    LazyLoadImageModule,
    HttpClientModule
  ],
  providers: [
    ApiEndPointsService,
    {provide: LAZYLOAD_IMAGE_HOOKS, useClass: ScrollHooks},
    {provide: APP_INITIALIZER,
      useFactory: (provider: ApiEndPointsService) => () => provider.load(),
      deps: [ ApiEndPointsService, HttpClient ], multi: true
      }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
