import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasketBuyerComponent } from './basket-buyer.component';

describe('BasketBuyerComponent', () => {
  let component: BasketBuyerComponent;
  let fixture: ComponentFixture<BasketBuyerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BasketBuyerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketBuyerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
