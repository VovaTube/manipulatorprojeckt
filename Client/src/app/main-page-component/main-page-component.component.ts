import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-page-component',
  templateUrl: './main-page-component.component.html',
  styleUrls: ['./main-page-component.component.css']
})
export class MainPageComponentComponent implements OnInit {

  atlas = '..//../assets/atlas.webp';
  phone = '..//..//assets/icons/phone.png';
  note = '..//..//assets/icons/note.png';
  money = '..//..//assets/icons/money.png';
  handhake = '..//..//assets/icons/hand-shake.png';
  truck = '..//..//assets/icons/truck.png';
  hook = '..//../assets/hook.webp';
  clients = 353;
  heavy = 420000;

  constructor() { }

  ngOnInit(): void {
    this.getDayOfYear();
  }
getDayOfYear(): void {
  const nowdate = Date.now();
  const dateyear =  Date.parse('01 Jan 2021 00:00:00 GMT');
  const difday = nowdate - dateyear;
  const days = Math.floor(difday / 1000 / 60 / 60 / 24);
  this.clients += days;
  if (days < 100)
  {
   this.heavy += days * 50;
  }
  else{
    this.heavy += days * 10;
  }
}

}
