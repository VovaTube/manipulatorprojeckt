import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-our-services',
  templateUrl: './our-services.component.html',
  styleUrls: ['./our-services.component.css']
})
export class OurServicesComponent implements OnInit {

  palet = '..//..//assets/serviceimg/palet.jpg';
  container = '..//..//assets/serviceimg/container.jpg';
  beton = '..//..//assets/serviceimg/beton.jpg';
  tools = '..//..//assets/serviceimg/tools.jpg';
  tehnika = '..//..//assets/serviceimg/tehnika.jpg';
  metal = '..//..//assets/serviceimg/metal.jpg';
  constructor() { }

  ngOnInit(): void {
  }

}
