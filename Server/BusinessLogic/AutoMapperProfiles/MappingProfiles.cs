﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using AutoMapper;
using DataAccsess.ModelsTable;
using BusinessLogic.ApiModels;
using DataAccsess.Enums;
using BusinessLogic.ApiModels.Category;

namespace BusinessLogic.AutoMapperProfiles
{
    public class MappingProfiles: Profile
    {
        public MappingProfiles()
        {
            CreateMap<ClientModel, FullOrderModel>()
                .ForMember(dest => dest.OrderCreated, opts => opts.MapFrom(src => DateTime.Now));

            CreateMap<AddCategoryModel, FullCategoryModel>()
                .ForMember(dest => dest.ImageId, opts => opts.MapFrom(src => src.ImageCategotyId))
               .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.CategoryName));

            CreateMap<AddSubCategoryModel, FullSubCategoryModel>()
               .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.SabName))
              .ForMember(dest => dest.CategoryId, opts => opts.MapFrom(src => src.categoryId));

            CreateMap<FullCategoryModel, ViewCategoryModel>();
            CreateMap<ViewCategoryModel, FullCategoryModel>();
            CreateMap<ViewCategoryModel, UpdateCategoryModel>();
            CreateMap<FullSubCategoryModel, ViewSubCategoryModel >();
            CreateMap<UpdateSubCategoryModel, ViewSubCategoryModel >();
        }
    }
}
