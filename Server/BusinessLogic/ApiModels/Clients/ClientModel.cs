﻿using DataAccsess.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.ApiModels
{
  public class ClientModel
    {
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string ClientCity { get; set;}
        public string ClientMail { get; set; }
    }
}
