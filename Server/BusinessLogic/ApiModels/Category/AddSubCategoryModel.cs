﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.ApiModels.Category
{
   public class AddSubCategoryModel
    {
        public string SabName { get; set; }
        public int categoryId { get; set; }
    }
}
