﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.ApiModels.Category
{
  public class UpdateCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ImageId { get; set; }
    }
}
