﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.ApiModels.Category
{
   public class UpdateSubCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
       
    }
}
