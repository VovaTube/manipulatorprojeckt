﻿using DataAccsess.ModelsTable;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.ApiModels.Category
{
   public class ViewSubCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public List<FullProductModel> ProductsInCategory { get; set; }
    }
}
