﻿using DataAccsess.ModelsTable;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.ApiModels.Category
{
   public class ViewCategoryModel
    {
        public int Id { get; set; }
        public  string Name { get; set; }
        public int ImageId { get; set; }
        public List<FullSubCategoryModel> SubCategory { get; set; }
    }
}
