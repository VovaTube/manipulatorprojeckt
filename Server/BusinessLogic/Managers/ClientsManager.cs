﻿using Autofac;
using AutoMapper;
using BusinessLogic.ApiModels;
using BusinessLogic.Managers.Interfaces;
using DataAccess.Repositories;
using DataAccsess.Enums;
using DataAccsess.ModelsTable;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using BusinessLogic.NotificationAlarm;
using BusinessLogic.NotificationAlarm.GmailModels;
using NLog;

namespace BusinessLogic.Managers
{
    public class ClientsManager : IClient
    {
        public IRepository<FullOrderModel> clientRepository { get; set; }
        public IMapper Mapper { get; set; }
        private GmailSender messageSender;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public ClientsManager()
        {
            messageSender = new GmailSender();
        }
        public Task<bool> AddNewClient(ClientModel model)
        {
            MessageModel newmessage = new MessageModel();
            newmessage.AdressFrom = "fb.your.choose@gmail.com";
            newmessage.AdressTo = "kotyubinvova@gmail.com";
            newmessage.MesssageHeader = $"У вас нове замовлення від {model.FirstName} перегляньте деталі";
            newmessage.MesssageText = $"Привіт мене звати {model.FirstName} я хочу"+" "+ "\n" + $"Мій телефон" + " "+ model.PhoneNumber;
            newmessage.AccauntPassword = "Kotyubin21031993";
            if(GmailSender.CheckForInternetConnection())
            {
                var fullarderModel = Mapper.Map<FullOrderModel>(model);
                bool flagadd = clientRepository.Add(fullarderModel);
                var x = clientRepository.Items;
                messageSender.SendMessage(newmessage);
                return Task.FromResult(flagadd);
            }
            else
            {
                Log.Info($"internet connection unavailable"+" "+DateTime.Now);
                return Task.FromResult(false);
            }
            
        }

        public async Task<bool> DeleteClient (FullOrderModel fullClientforDelete)
        {
            var deleteClientFlag = await clientRepository.DeleteAsync(fullClientforDelete);
            return deleteClientFlag;
        }

        public Task<List<FullOrderModel>> GetAllClients()
        {
            var entytyes = clientRepository.Items;
            return Task.FromResult(entytyes.ToList());
        }

        public Task<List<FullOrderModel>> GetAllNewClients()
        {
            var entytyes = clientRepository.Items.Where(s => s.OrderStasus == OrderStatus.NewOrder);
            return Task.FromResult(entytyes.ToList());
        }

        public Task <FullOrderModel> GetClientById(int Id)
        {
            var entytyes = clientRepository.Items.Where(s => s.Id == Id).FirstOrDefault();
            return Task.FromResult(entytyes);
        }

        public  Task<bool> UpdateClient(FullOrderModel fullClient)
        {
            //var client = clientRepository.Items.FirstOrDefault(el => el.Id == fullClient.Id);
            //client.f = fullClient.FirstName;
            //client.LastName = fullClient.LastName;
            //client.СontractDate = fullClient.СontractDate;
            //client.KindofWorks = fullClient.KindofWorks;
            //client.OrderStasus = fullClient.OrderStasus;
            //client.MobilePhone = fullClient.MobilePhone;
            //var flagudate = clientRepository.SaveChanges();
            //clientRepository.ClearAllEnteties();
            return Task.FromResult(true);
        }
    }
}
