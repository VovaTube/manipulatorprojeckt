﻿using BusinessLogic.ApiModels.Category;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Managers.Interfaces
{
   public interface ICategories
    {
        public Task<bool> AddNewCategory(AddCategoryModel addCategory);
        public Task<bool> DeleteCategory(int idCategory);
        public Task<ViewCategoryModel> UpdateCategory(UpdateCategoryModel updatedCategory);
        public Task<List<ViewCategoryModel>> GetAllCategories();
        public Task<ViewCategoryModel> GetCategoryById(int id);
        public Task<bool> AddSubNewCategory(AddSubCategoryModel addCategory);
        public Task<bool> DeleteSubCategory(int idSubCategory);
        public Task<ViewSubCategoryModel> UpdateSubCategory(UpdateSubCategoryModel updatedCategory);
        public Task<ViewSubCategoryModel> GetSubCategoriesById( int Id);
    }
}
