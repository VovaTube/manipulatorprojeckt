﻿using DataAccsess.ModelsTable;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Managers.Interfaces
{
   public interface IProducts
    {
        public Task<bool> AddNewProducts();
        public Task<List<FullProductModel>> GetAllProductsByCategory(int categoryid);
        public Task<List<FullProductModel>> GetAllProductsBySybCategory(int subcategoryid);
        public Task<List<FullCategoryModel>> GetAllCategory();
        public Task<List<FullSubCategoryModel>> GetAllSubCategory(int categoryid);
        public Task<FullOrderModel> GetClientById(int Id);
        public Task<bool> UpdateClient(FullOrderModel fullClient);
        public Task<bool> DeleteClient(FullOrderModel fullClient);
    }
}
