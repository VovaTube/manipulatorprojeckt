﻿using BusinessLogic.ApiModels;
using DataAccsess.ModelsTable;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Managers.Interfaces
{
    public interface IClient
    {
       public Task<bool> AddNewClient(ClientModel model);
       public Task <List<FullOrderModel>> GetAllClients();
       public Task<List<FullOrderModel>> GetAllNewClients();
       public Task <FullOrderModel> GetClientById(int Id);
       public Task <bool> UpdateClient (FullOrderModel fullClient);
       public Task<bool> DeleteClient(FullOrderModel fullClient);
    }
}
