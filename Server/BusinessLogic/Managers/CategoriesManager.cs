﻿using AutoMapper;
using BusinessLogic.ApiModels.Category;
using BusinessLogic.Managers.Interfaces;
using DataAccess.Repositories;
using DataAccsess.ModelsTable;
using Microsoft.EntityFrameworkCore;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Managers
{
    class CategoriesManager : ICategories
    {
        public IRepository<FullCategoryModel> categoriesRepository { get; set; }
        public IRepository<FullSubCategoryModel> subcategoriesRepository { get; set; }
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public IMapper Mapper { get; set; }
        public async Task<bool> AddNewCategory(AddCategoryModel addCategory)
        {
            var fullModel = Mapper.Map<FullCategoryModel>(addCategory);
           int x =  await categoriesRepository.AddAsync(fullModel);
            if (x > 0)
            {
                return true;
            }
            else return false;
        }

        public async Task<bool> AddSubNewCategory(AddSubCategoryModel addsubCategory)
        {
            var fullModel = Mapper.Map<FullSubCategoryModel>(addsubCategory);
            int x = await subcategoriesRepository.AddAsync(fullModel);
            if (x > 0)
            {
                return true;
            }
            else return false;
        }

        public async Task<bool> DeleteCategory(int idCategory)
        {
            var dboCategory = await categoriesRepository.Items.Where(el=>el.Id== idCategory).Include(item => item.SubCategory).FirstOrDefaultAsync();
            if (dboCategory == null)
            {
                Log.Info("Даних в БД не знайдено");
                return false;
            }
            if (dboCategory.SubCategory.Count>0)
            {
                Log.Info("Дана категорія містить під катеогії, її видалення неможливе");
                return false;
            }
            else
            {
              var flag =  await categoriesRepository.DeleteAsync(dboCategory);
                return flag;
            }
            
        }

        public async Task<bool> DeleteSubCategory(int idSubCategory)
        {
            var subcat = await subcategoriesRepository.Items.Where(el => el.Id == idSubCategory).Include(el=>el.ProductsInCategory).FirstOrDefaultAsync();
            var delflag = await subcategoriesRepository.DeleteAsync(subcat);
            return delflag;

        }

        public async Task<List<ViewCategoryModel>> GetAllCategories()
        {
            var dboCategory =  await categoriesRepository.Items.Include(el=>el.SubCategory).AsNoTracking().ToListAsync();
            var viewModel = Mapper.Map<List<ViewCategoryModel>>(dboCategory);
            return viewModel;
        }


        public async Task<ViewCategoryModel> GetCategoryById( int id)
        {
            var category = await categoriesRepository.Items.Where(etems => etems.Id == id).Include(el => el.SubCategory).FirstOrDefaultAsync();
            var catMap = Mapper.Map<ViewCategoryModel>(category); 
            return catMap;
        }

        public async Task<ViewSubCategoryModel> GetSubCategoriesById( int id)
        {
            var etems = await subcategoriesRepository.Items.Where(el => el.Id == id).Include(el => el.ProductsInCategory).FirstOrDefaultAsync();

            if (etems!=null)
            {
                var mapItems = Mapper.Map<ViewSubCategoryModel>(etems);
                return mapItems;
            }
            else
            {
                var mapItems = Mapper.Map<ViewSubCategoryModel>(etems);
                return mapItems;
            }
            
        }

        public async Task<ViewCategoryModel> UpdateCategory(UpdateCategoryModel updatedCategory)
        {
            var dboEntyty = await categoriesRepository.Items.Where(item => item.Id == updatedCategory.Id).FirstOrDefaultAsync();
            if (dboEntyty==null)
            {
                Log.Info($"Категорія '{updatedCategory.Name}' відсутня в базі даних ");
                var viewModel = Mapper.Map<ViewCategoryModel>(dboEntyty);
                return viewModel;
            }
            else
            {
                dboEntyty.ImageId = updatedCategory.ImageId;
                dboEntyty.Name = updatedCategory.Name;
                categoriesRepository.SaveChanges();
                var viewModel = Mapper.Map<ViewCategoryModel>(dboEntyty);
                return viewModel;
            }
        }

        public async Task<ViewSubCategoryModel> UpdateSubCategory(UpdateSubCategoryModel updatedSubCategory)
        {
            var itemsSub = await subcategoriesRepository.Items.Where(el => el.Id == updatedSubCategory.Id).FirstOrDefaultAsync();
            if (itemsSub!=null)
            {
                itemsSub.Name = updatedSubCategory.Name;
                var updateflag = await subcategoriesRepository.UpdateAsync(itemsSub);
                var mapSub = Mapper.Map<ViewSubCategoryModel>(itemsSub);
                if (updateflag)
                {
                    return mapSub;
                }
                else
                {
                    var viewModel = Mapper.Map<ViewSubCategoryModel>(itemsSub);
                    return viewModel;
                }
               

            }
            else
            {
                Log.Info($"Підкатегорія '{updatedSubCategory.Name}' відсутня в базі даних ");
                var viewModel = Mapper.Map<ViewSubCategoryModel>(itemsSub);
                return viewModel;
            }
        }
    }
}
