﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using BusinessLogic.Managers;
using BusinessLogic.Managers.Interfaces;

namespace BusinessLogic
{
    public static class RegisterBusinessLogic
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<ClientsManager>().As<IClient>().InstancePerLifetimeScope().PropertiesAutowired();
            builder.RegisterType<CategoriesManager>().As<ICategories>().InstancePerLifetimeScope().PropertiesAutowired();

        }
}
}
