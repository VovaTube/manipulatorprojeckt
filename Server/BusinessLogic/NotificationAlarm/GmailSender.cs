﻿using BusinessLogic.NotificationAlarm.GmailModels;
using EASendMail;
using NLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.NotificationAlarm
{
    public class GmailSender
    {
        private SmtpServer Server;
        private SmtpMail Mail;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public GmailSender()
        {
            Server = new SmtpServer("smtp.gmail.com");
            Mail = new SmtpMail("TryIt");
        }
        public Task<bool> SendMessage(MessageModel message)

        {
            try
            {
                Mail.From = message.AdressFrom;
                // Set recipient email address
                Mail.To = message.AdressTo;
                // Set email subject
                Mail.Subject = message.MesssageHeader;
                // Set email body
                Mail.TextBody = message.MesssageText;
                // Gmail user authentication
                // For example: your email is "gmailid@gmail.com", then the user should be the same
                Server.User = message.AdressFrom;
                Server.Password = message.AccauntPassword;
                // Set 465 port
                Server.Port = 465;
                // detect SSL/TLS automatically
                Server.ConnectType = SmtpConnectType.ConnectSSLAuto;
                SmtpClient oSmtp = new SmtpClient();
                oSmtp.SendMail(Server, Mail);
                Log.Info($"Send massage to {message.AdressTo}");
                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                Log.Info($"Send massage error {ex.Message}");
                return Task.FromResult(false);
            }

        }
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("https://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }

        }
    }
}
