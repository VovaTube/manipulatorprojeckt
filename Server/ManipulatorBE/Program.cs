using Autofac.Extensions.DependencyInjection;
using DataAccsess.DbInitializer;
using DataAccsess.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ManipulatorBE
{
    public class Program
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            CreateDbIfNotExists(host);
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webHostBuilder =>
                    {
                         webHostBuilder
                        .UseIISIntegration()
                        .UseStartup<Startup>()
                        .ConfigureLogging((hostingContext, logging) =>
                            {
                                logging.AddNLog(hostingContext.Configuration.GetSection("Logging"));
                            });

                    });

        private static void CreateDbIfNotExists(IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<ManipulatorContext>();
                   // context.Database.EnsureCreated();
                    DbInitializerDebug.Initialize(context);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    Log.Info(ex, "An error occurred creating the DB.");
                }
            }
        }
    }

}
