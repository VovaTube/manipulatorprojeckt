﻿using DataAccsess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManipulatorBE.Extenstions
{
    public static class ServisesExtenstion
    {
        public static void ConfigureDb(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ManipulatorContext>(options =>
            {
                options.UseSqlServer(connectionString, b => b.MigrationsAssembly("ManipulatorBE"));
                options.EnableSensitiveDataLogging(true);
            });
        }
    }
}
