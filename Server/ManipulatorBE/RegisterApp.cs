﻿using Autofac;
using AutoMapper;
using DataAccess.Repositories;
using DataAccsess.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace ManipulatorBE
{
    public static class RegisterApp
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<ManipulatorContext>().As<DbContext>().InstancePerLifetimeScope();
            // Реєструємо об'єктконтроллера для отримання його через автосвойства а не конструктор
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(t => typeof(ControllerBase).IsAssignableFrom(t)).PropertiesAutowired();
            builder.RegisterType<ManipulatorContext>().Named<DbContext>("NewInstance").InstancePerDependency();
            // Реєструємо об'єкт репозипорію бази даних
            builder.RegisterGeneric(typeof(SqlRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope().PropertiesAutowired();
           
            // Реєструємо автомапер
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            builder.RegisterAssemblyTypes(assemblies).Where(t => typeof(Profile).IsAssignableFrom(t) && !t.IsAbstract && t.IsPublic).As<Profile>().PropertiesAutowired();

            builder.Register(c => new MapperConfiguration(cfg =>
            {
                foreach (var profile in c.Resolve<IEnumerable<Profile>>())
                {
                    cfg.AddProfile(profile);
                }
            })).AsSelf().AutoActivate().SingleInstance().PropertiesAutowired();

            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve)).As<IMapper>().SingleInstance().PropertiesAutowired();

        }
    }
}
