﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using System.Threading.Tasks;

namespace ManipulatorBE
    
{
    public class ServiseModules : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterApp.RegisterTypes(builder);
            BusinessLogic.RegisterBusinessLogic.RegisterTypes(builder);
        }
    }
}
