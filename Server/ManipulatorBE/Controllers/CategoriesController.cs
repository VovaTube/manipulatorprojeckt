﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Managers;
using BusinessLogic.ApiModels;
using BusinessLogic.Managers.Interfaces;
using NLog;
using Autofac;
using AutoMapper;
using DataAccsess.Enums;
using DataAccsess.ModelsTable;
using BusinessLogic.ApiModels.Category;

namespace ManipulatorBE.Controllers
{
    [Route("category")]
    [ApiController]
    public class CategoriesController:ControllerBase
    {
        public ICategories categoriesManeger { get; set; }
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private ILifetimeScope _container;

        //нова категорія
        [HttpPost("addnewcategory")]
        public async Task<ActionResult<bool>> addNewCategory(AddCategoryModel addCategoryModel)
        {
            bool addflag = await categoriesManeger.AddNewCategory(addCategoryModel);
            return addflag;

        }

        //видалити категорію за id
        [HttpGet("deletecategory")]
        public async Task<ActionResult<bool>> delCategory(int idCategory)
        {
            var addflag = await categoriesManeger.DeleteCategory(idCategory);
            return false;

        }

        //оновити категорію
        [HttpPost("updatecategory")]
        public async Task<ActionResult<ViewCategoryModel>> updateCategory(UpdateCategoryModel updateModel)
        {
            var updateflag = await categoriesManeger.UpdateCategory(updateModel);
            return updateflag;

        }

        //отримати всі категорії
        [HttpGet("getcategories")]
        public async Task<ActionResult<IList<ViewCategoryModel>>> getAllCategory()
        {
            var addflag = await categoriesManeger.GetAllCategories();
            if (addflag.Count>0)
            {
                return Ok(addflag); ;
            }
            else
            {
                return NotFound();
            }
        }

        //отримати категорію за Id
        [HttpGet("getcategoriesbyid")]
        public async Task<ActionResult<IList<ViewCategoryModel>>> getAllCategoryById( int id)
        {
            var item = await categoriesManeger.GetCategoryById(id);
            if (item != null)
            {
                return Ok(item);
            }
            else
            {
                return NotFound();
            }
        }

        //нова підкатегорія
        [HttpPost("addnewsubcategory")]
        public async Task<ActionResult<bool>> addNewSubCategory(AddSubCategoryModel addCategoryModel)
        {
            bool addflag = await categoriesManeger.AddSubNewCategory(addCategoryModel);
            return addflag;

        }

        //виделення підкатегорії
        [HttpGet("delsubcategory")]
        public async Task<ActionResult<bool>> deleteSubCategory(int  id)
        {
            bool addflag = await categoriesManeger.DeleteSubCategory(id);
            return addflag;
        }

        //оновити підкатегорію
        [HttpPost("updatesubcategory")]
        public async Task<ActionResult<ViewSubCategoryModel>> updateSubCategory(UpdateSubCategoryModel updateModel)
        {
            var updateflag = await categoriesManeger.UpdateSubCategory(updateModel);
            return updateflag;

        }

        //отримати всі підкатегорії
        [HttpGet("getsubcategories")]
        public async Task<ActionResult<bool>> getSubCategory(int idsubCategory)
        {
            var addflag = await categoriesManeger.GetAllCategories();
            return false;

        }

        //отримати підкатегорію по id
        [HttpGet("getsubcategorybyid")]
        public async Task<ActionResult<ViewSubCategoryModel>> getSubCategoryById(int idsubCategory)
        {
            var subCat = await categoriesManeger.GetSubCategoriesById(idsubCategory);
            return subCat;

        }


    }
}
