﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Managers;
using BusinessLogic.ApiModels;
using BusinessLogic.Managers.Interfaces;
using NLog;
using Autofac;
using AutoMapper;
using DataAccsess.Enums;
using DataAccsess.ModelsTable;

namespace ManipulatorBE.Controllers
{
    [Route("clients")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        public IClient clientManeger { get; set; }
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private ILifetimeScope _container;
        public IMapper Mapper { get; set; }
        public ClientController(ILifetimeScope container)
        {
            this._container = container;
        }
        [HttpGet("test")]
        public bool Test ()
        {
            return true;
        }

        [HttpPost("addnewclient")]
        public async Task<ActionResult<bool>> addNewClients(ClientModel model)
        {
            //ClientModel model = new ClientModel();
            //model.FirstName = "Дмитро";
            //model.LastName = "Коросташов";
            //model.PhoneNumber = "0680071634";
            //model.WorkType = KindofWorks.CastomDelivery; 
            bool addclientflag =  await clientManeger.AddNewClient(model);
            return addclientflag;

        }
        [HttpGet("getallclients")]
        public async Task<List<FullOrderModel>> getAllClients()
        {
            var allclients = await clientManeger.GetAllClients();
            return allclients;
        }

        [HttpGet("getnewclients")]
        public async Task<List<FullOrderModel>> getAllNewClients()
        {
            var allnewclients = await clientManeger.GetAllNewClients();
            return allnewclients;
        }

        [HttpGet("getclientbyid")]
        public async Task<FullOrderModel> getClientById(int Id )
        {
            var allnewclients = await clientManeger.GetClientById(Id);
            return allnewclients;
        }

        [HttpPost("updateclient")]
        public async Task<bool> updateClient(FullOrderModel fullClient)
        {
            var udateresult = await clientManeger.UpdateClient(fullClient);
            return udateresult;
        }

        [HttpPost("deleteclient")]
        public async Task<bool> deleteClient(FullOrderModel fullClient)
        {
            var deleteresualt = await clientManeger.DeleteClient(fullClient);
            return deleteresualt;
        }
    }
}
