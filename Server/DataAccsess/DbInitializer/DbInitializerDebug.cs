﻿using DataAccsess.Models;
using DataAccsess.ModelsTable;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace DataAccsess.DbInitializer
{
   public  class DbInitializerDebug
    {
        private IHostEnvironment _hostingEnv;
        public static void Initialize(ManipulatorContext context)
        {
            context.Database.EnsureCreated();

            // Look for any etyties.
            if (context.Category.Any())
            {
                return;   // DB has been seeded
            }
            #region Категорії
            FullCategoryModel [] defaultCategories = new  FullCategoryModel []
            {
                new FullCategoryModel
                {
                   // Id=1,
                    Name="Електроніка",
                    ImageId=1,
                },
                new FullCategoryModel
                {
                    //Id=2,
                    Name="Техінка для кухні",
                    ImageId=2,
                },
                new FullCategoryModel
                {
                   // Id=3,
                    Name="Товари для дому",
                    ImageId=3,
                },
                new FullCategoryModel
                {
                   // Id=4,
                    Name="Дім, сад город",
                    ImageId=4,
                },
                new FullCategoryModel
                {
                  //  Id=5,
                    Name="Музикальні інструменти",
                    ImageId=5,
                },
                new FullCategoryModel
                {
                    //Id=6,
                    Name="Іграшки",
                    ImageId=6,
                },
                new FullCategoryModel
                {
                    //Id=7,
                    Name="Косметика",
                    ImageId=7,
                },
                new FullCategoryModel
                {
                    //Id=8,
                    Name="Атомобільні товари",
                    ImageId=8,
                },
                new FullCategoryModel
                {
                    //Id=9,
                    Name="Товари для офісу",
                    ImageId=9,
                },
                new FullCategoryModel
                {
                    //Id=10,
                    Name="Особиста гігієна",
                    ImageId=10,
                },

            };

            context.Category.AddRange(defaultCategories);
            context.SaveChanges();
            #endregion
            #region ПідКатегорії

            FullSubCategoryModel [] defaultSubCategories = new FullSubCategoryModel []
            {
                #region Електроніка
                 new FullSubCategoryModel
                {
                    //Id=1,
                    Name="Телефони",
                    CategoryId = 1,
                },
                new FullSubCategoryModel
                {
                   // Id=2,
                    Name="Компютери",
                    CategoryId = 1,
                },
                 new FullSubCategoryModel
                {
                  //  Id=3,
                    Name="Ноутбуки",
                    CategoryId = 1,
                },
                 new FullSubCategoryModel
                {
                   // Id=4,
                    Name="Монітори",
                    CategoryId = 1,
                },
                 new FullSubCategoryModel
                {
                  //  Id=5,
                    Name="Приналежності",
                    CategoryId = 1,
                },
                #endregion
            #region Техніка для кухні
                 new FullSubCategoryModel
                {
                    //Id=6,
                    Name="Холодильники",
                    CategoryId = 2,
                },
                  new FullSubCategoryModel
                {
                    //Id=7,
                    Name="Чайники",
                    CategoryId = 2,
                },
                   new FullSubCategoryModel
                {
                   // Id=8,
                    Name="Микрохвильові печі",
                    CategoryId = 2,
                },
                    new FullSubCategoryModel
                {
                    //Id=9,
                    Name="Посудомийки",
                    CategoryId = 2,
                },
                     new FullSubCategoryModel
                {
                   // Id=10,
                    Name="Умивальники",
                    CategoryId = 2,
                },
	            #endregion
                      #region Товари для дому
                 new FullSubCategoryModel
                {
                  //  Id=11,
                    Name="Садовий інвентар",
                    CategoryId = 3,
                },
                  new FullSubCategoryModel
                {
                  //  Id=12,
                    Name="Інвентар для прибирання",
                    CategoryId = 3,
                },
                   new FullSubCategoryModel
                {
                   // Id=13,
                    Name="Насоси",
                    CategoryId = 3,
                },
                    new FullSubCategoryModel
                {
                   // Id=14,
                    Name="Газон",
                    CategoryId = 3,
                },
                     new FullSubCategoryModel
                {
                   // Id=15,
                    Name="Меблі",
                    CategoryId = 3,
                },
	            #endregion



            };
            context.SubCategory.AddRange(defaultSubCategories);
            context.SaveChanges();
            #endregion 
        }
    }
}
