﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IRepository<TEntity>: IDisposable where TEntity : class 
    {
        bool Add(TEntity entity);
        Task<int> AddAsync(TEntity entity);
        bool Add(IEnumerable<TEntity> entities);
        Task<bool> AddAsync(IEnumerable<TEntity> entities);
        bool Update(TEntity entity);
        Task<bool> UpdateAsync(TEntity entity);
        bool Delete(TEntity entity);
        Task<bool> DeleteAsync(TEntity entity);
        bool Delete(IEnumerable<TEntity> entities);
        Task<bool> DeleteAsync(IEnumerable<TEntity> entities);
        IQueryable<TEntity> Items { get; }
        TEntity Find(object id);
        Task<TEntity> FindAsync(object id);
        TEntity FindBy(Expression<Func<TEntity, bool>> findExpression);
        Task<TEntity> FindByAsync(Expression<Func<TEntity, bool>> findExpression);
        IQueryable<TEntity> FilterBy(Expression<Func<TEntity, bool>> filterExpression);
        bool SaveChanges();
        /// <summary>
        /// Отримати трекер всіх відслідковуємих обьектів
        /// </summary>
        /// <returns></returns>
        ChangeTracker GetallChangesTracker();
        /// <summary>
        /// Отримати всі відслідковувані обьекти для цієї таблиці
        /// </summary>
        /// <returns></returns>
        IEnumerable<EntityEntry<TEntity>> GetTrackingEnteties();
        /// <summary>
        /// Припинити відслідковування всіх обьєктів даної таблиці, які не було змінено
        /// </summary>
        /// <returns></returns>
        bool ClearUnchangesEnteties();
        /// <summary>
        /// Припинити відслідковування всіх обьєктів даної таблиці, які було видалено
        /// </summary>
        /// <returns></returns>
        public bool ClearDeletedEnteties();
        /// <summary>
        /// Припинити відслідковування всіх обьєктів даної таблиці
        /// </summary>
        /// <returns></returns>
        bool ClearAllEnteties();
    }
}
