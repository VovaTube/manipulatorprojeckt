﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DataAccess.Repositories
{
    public class SqlRepository<TEntity> : IRepository<TEntity>
    where TEntity : class
    {
        public DbContext DbContext { get; set; }

        public bool Add(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
            DbContext.SaveChanges();
            return true;
        }

        public async Task<int> AddAsync(TEntity entity)
        {
            await DbContext.Set<TEntity>().AddAsync(entity);
            return await DbContext.SaveChangesAsync();
        }

        public bool Add(IEnumerable<TEntity> entities)
        {
            DbContext.Set<TEntity>().AddRange(entities);
            DbContext.SaveChanges();
            return true;
        }

        public async Task<bool> AddAsync(IEnumerable<TEntity> entities)
        {
            await DbContext.Set<TEntity>().AddRangeAsync(entities);
            await DbContext.SaveChangesAsync();
            return true;
        }

        public bool Update(TEntity entity)
        {
            DbContext.Set<TEntity>().Update(entity);
            DbContext.SaveChanges();
            return true;
        }

        public async Task<bool> UpdateAsync(TEntity entity)
        {
            DbContext.Set<TEntity>().Update(entity);
            await DbContext.SaveChangesAsync();
            return true;
        }

        public bool Delete(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
            DbContext.SaveChanges();
            return true;
        }

        public async Task<bool> DeleteAsync(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
            await DbContext.SaveChangesAsync();
            return true;
        }

        public bool Delete(IEnumerable<TEntity> entities)
        {
            DbContext.Set<TEntity>().RemoveRange(entities);
            DbContext.SaveChanges();
            return true;
        }

        public async Task<bool> DeleteAsync(IEnumerable<TEntity> entities)
        {
            DbContext.Set<TEntity>().RemoveRange(entities);
            await DbContext.SaveChangesAsync();
            return true;
        }

        public IQueryable<TEntity> Items => DbContext.Set<TEntity>();

        public TEntity Find(object id)
        {
            return DbContext.Set<TEntity>().Find(id);
        }

        public async Task<TEntity> FindAsync(object id)
        {
            return await DbContext.Set<TEntity>().FindAsync(id);
        }

        public TEntity FindBy(Expression<Func<TEntity, bool>> expression)
        {
            return FilterBy(expression).FirstOrDefault();
        }

        public async Task<TEntity> FindByAsync(Expression<Func<TEntity, bool>> expression)
        {
            return await FilterBy(expression).FirstOrDefaultAsync();
        }

        public IQueryable<TEntity> FilterBy(Expression<Func<TEntity, bool>> expression)
        {
            return DbContext.Set<TEntity>().Where(expression);
        }

        public bool SaveChanges()
        {
            DbContext.SaveChanges();
            return true;
        }

        public ChangeTracker GetallChangesTracker()
        {
            return DbContext.ChangeTracker;
        }

        public IEnumerable<EntityEntry<TEntity>> GetTrackingEnteties()
        {
            return DbContext.ChangeTracker.Entries<TEntity>();
        }

        public bool ClearUnchangesEnteties()
        {
            var enteties = GetTrackingEnteties()
                .Where(ent => ent.State == EntityState.Unchanged);

            foreach (var entetie in enteties)
            {
                entetie.State = EntityState.Detached;
            }

            return true;
        }

        public bool ClearDeletedEnteties()
        {
            var enteties = GetTrackingEnteties()
                .Where(ent => ent.State == EntityState.Deleted);

            foreach (var entetie in enteties)
            {
                entetie.State = EntityState.Detached;
            }

            return true;
        }

        public bool ClearAllEnteties()
        {
            var enteties = GetTrackingEnteties();

            foreach (var entetie in enteties)
            {
                entetie.State = EntityState.Detached;
            }

            return true;
        }

        public void Dispose()
        {
            this.DbContext.Dispose();
        }
    }
}