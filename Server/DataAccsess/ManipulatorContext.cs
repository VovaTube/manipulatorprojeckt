﻿using DataAccsess.DbInitializer;
using DataAccsess.ModelsTable;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;


namespace DataAccsess.Models
{
    public class ManipulatorContext : DbContext

    {
        private IHostEnvironment _hostingEnv;
        public DbSet<FullOrderModel> Orders { get; set; }
        public DbSet<FullCategoryModel> Category { get; set; }
        public DbSet<FullClientModel> Clients { get; set; }
        public DbSet<FullProdImageModel> Images { get; set; }
        public DbSet<FullProductModel> Products { get; set; }
        public DbSet<FullProductPropertiesModel> ProdProperties { get; set; }
        public DbSet<FullSubCategoryModel> SubCategory { get; set; }
        public ManipulatorContext(DbContextOptions<ManipulatorContext> options, IHostEnvironment hostingEnv)
            : base(options)
        {
            _hostingEnv = hostingEnv;
        }

        protected override async void OnModelCreating(ModelBuilder modelBuilder)
        {
                 modelBuilder.Entity<FullProductPropertiesModel>()
                .HasOne(p => p.FullProductModel)
                .WithMany(prod => prod.ProdProperties)
                .HasForeignKey(key => key.ProductId)
                .OnDelete(DeleteBehavior.NoAction);

                modelBuilder.Entity<FullProdImageModel>()
                .HasOne(p => p.FullProductModel)
                .WithMany(prod => prod.ProdImages)
                .HasForeignKey(key => key.ProductId)
                .OnDelete(DeleteBehavior.NoAction);

                 modelBuilder.Entity<FullProductModel>()
                .HasOne(p => p.FullSubCategoryModel)
                .WithMany(listprod => listprod.ProductsInCategory)
                .HasForeignKey(key => key.SubCategoryId)
                .OnDelete(DeleteBehavior.NoAction);

                modelBuilder.Entity<FullSubCategoryModel>()
               .HasOne(p => p.FullCategoryModel)
               .WithMany(listprod => listprod.SubCategory)
               .HasForeignKey(key => key.CategoryId)
               .OnDelete(DeleteBehavior.Cascade);

           

           
        }
    }
}
