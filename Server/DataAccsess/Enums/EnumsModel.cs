﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccsess.Enums
{
   public enum OrderStatus
    {
        NewOrder,
        Confirmed,
        Complited,
        Declimed
    }
    public enum KindofWorks
    {
        DeliveryMaterials,
        DeliveryTechnics,
        DeliveryBetomMaterials,
        CastomDelivery
    }
}
