﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccsess.ModelsTable
{
  public class FullProdImageModel
    {
        [Column("ImageId")]
        public int Id { get; set;}
        public virtual string ImageSourse { get; set;}
        public int ProductId { get; set; }
        public FullProductModel FullProductModel { get; set; }
    }
}
