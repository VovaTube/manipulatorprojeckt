﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccsess.ModelsTable
{
   public class FullProductModel
    {
        [Column("ProductId")]
        public int Id { get; set;}
        public string Name { get; set;}
        public int SubCategoryId { get; set; }
        public FullSubCategoryModel FullSubCategoryModel;
        public virtual string Description { get; set;}
        public virtual string DescriptionFooter { get; set;}
        public virtual int Price { get; set;}
        public virtual string FolderImageNane { get; set; }
        public virtual bool Discount { get; set; }
        public List<FullProductPropertiesModel> ProdProperties { get; set; }
        public List<FullProdImageModel> ProdImages { get; set; }

    }
}
