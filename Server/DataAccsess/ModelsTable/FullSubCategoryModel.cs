﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccsess.ModelsTable
{
    public class FullSubCategoryModel
    {
        [Column("SubCategoryId")]
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public FullCategoryModel FullCategoryModel;
        public List<FullProductModel> ProductsInCategory { get; set; }
    }
}
