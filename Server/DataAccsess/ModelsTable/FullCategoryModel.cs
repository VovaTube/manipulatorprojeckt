﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccsess.ModelsTable
{
    public class FullCategoryModel
    {
        [Column("CategoryId")]
        public int Id { get; set; }
        public virtual string Name {get; set;}
        public int ImageId { get; set;}
        public List<FullSubCategoryModel> SubCategory {get; set;}
        public FullProdImageModel FullProdImageModel { get; set; }




    }
}
