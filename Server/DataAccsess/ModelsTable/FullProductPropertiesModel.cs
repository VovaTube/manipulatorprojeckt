﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccsess.ModelsTable
{
    public class FullProductPropertiesModel
    {
        [Column("PropId")]
        public int Id { get; set; }
        public virtual string PropertyName { get; set; }
        public virtual string PropertyValue { get; set; }
        public int ProductId { get; set; }
        public FullProductModel FullProductModel { get; set;}

    }
}
