﻿using DataAccsess.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccsess.ModelsTable
{
   public class FullOrderModel
    {
        public int Id { get; set; }
        public virtual DateTime OrderCreated {get; set;}
        public virtual DateTime СontractDate {get; set;}
        public virtual OrderStatus OrderStasus {get; set;}
        public virtual KindofWorks KindofWorks {get; set;}
        public virtual int ClientId { get; set;}
        public virtual FullClientModel FullClientModel {get; set;}
        public List<FullProductModel> OrderPruducts { get; set; }


    }
}
