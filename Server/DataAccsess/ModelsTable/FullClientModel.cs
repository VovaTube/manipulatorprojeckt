﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccsess.ModelsTable
{
   public class FullClientModel
    {
        [Column("ClientId")]
        public int Id {get; set;}
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string ClientEmail { get; set;}
        public List<FullOrderModel> Orders { get; set; }

    }
}
